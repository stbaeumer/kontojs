class GiroKonto extends Konto {
    constructor(ktoNr, ktoInhaber, ktoSaldo, dispo) {
        super(ktoNr, ktoInhaber, ktoSaldo)
        this.dispo = dispo
    }
    abheben (betrag) {
        if (this.ktoSaldo - betrag >= - this.dispo ) {
            super.abheben(betrag)
            return true
        } else {
            return false
        }
    }  
    toString() {
        return super.toString() + `<br> Dispositionskredit: ${this.dispo.toString()}`
    }
}