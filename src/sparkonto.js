class SparKonto extends Konto {
    constructor(ktoNr, ktoInhaber, ktoSaldo, zinsSatz) {
        super(ktoNr, ktoInhaber, ktoSaldo)
        this.zinsSatz = zinsSatz
    }
    
    abheben (betrag) {
        if (this.ktoSaldo - betrag >= 0 ) {
            super.abheben(betrag)
            return true
        } else {
            return false
        }
    }  
    
    toString() {
        return super.toString() + `<br> Zinssatz: ${this.zinsSatz.toString()} %`
    }
}