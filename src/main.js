let konto
function kontoErzeugen() {
    document.getElementById("ausgabe").innerHTML = ``
    const wahl = document.getElementById("girokonto").checked
    const ktoNr = document.getElementById("ktonr").value
    const ktoInh =  document.getElementById("ktoInh").value
    const saldo = parseFloat(document.getElementById("saldo").value)
    if ( wahl ) {
        const dispo = 2000  // Dies kann später auch als Eingabefeld ergänzt werden
        konto = new GiroKonto(ktoNr, ktoInh, saldo, dispo)
        console.log(konto)        
    }
    else{
        const zinsSatz = 0.1 //  Dies kann später auch als Eingabefeld ergänzt werden
        konto = new SparKonto(ktoNr, ktoInh, saldo, zinsSatz)
        console.log(konto)
    }
    ausgeben(konto)
}

function ausgeben(kto) {
    document.getElementById("ausgabe").innerHTML = kto.toString()
}

function betragAuslesen() {
    return parseFloat(document.getElementById("betrag").value)
}

function abheben () {
    const betrag = betragAuslesen()
    const istOk = konto.abheben(betrag)  
    if (istOk) {
        ausgeben(konto)
    } else {
        document.getElementById("ausgabe").innerHTML = 'Abhebung nicht möglich'
    }
    console.log(konto)
}

function einzahlen() {
    const betrag = betragAuslesen()
    konto.einzahlen(betrag)
    ausgeben(konto)
}
 
document.getElementById("btnKontoErzeugen").addEventListener("click",kontoErzeugen)
document.getElementById("btnAuszahlen").addEventListener("click", abheben)
document.getElementById("btnEinzahlen").addEventListener("click", einzahlen)
