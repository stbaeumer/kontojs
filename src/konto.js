class Konto {
    constructor(ktoNr, ktoInhaber, ktoSaldo) {
        this.ktoNr= ktoNr
        this.ktoInhaber = ktoInhaber
        this.ktoSaldo = ktoSaldo
    }
    
    abheben(betrag) {
         this.ktoSaldo -= betrag
         return true
    }
    
    einzahlen(betrag) {    
        this.ktoSaldo += betrag
    }
    
    toString() {
       return `Kontonummer: ${this.ktoNr} <br>
               Kontoinhaber: ${this.ktoInhaber} <br>
               Saldo: ${this.ktoSaldo.toString()}`
    }
}